app.controller('DashboardController', ['$scope', function($scope) {
  // database.then(function(data) {
  // });
  $scope.currentLetter = '';
  $scope.currentWord = '';
  $scope.word = ['m', 'o', 'o', 'n'];
  $scope.count = $scope.word.length;
  $scope.index = 0;
  $scope.log = [];

  $scope.check = function(letter, e) {
    if ($scope.index === $scope.count) {
      alert('YAY!');
      $scope.index = 0;
      $scope.currentWord = '';
      $scope.log = [];
    }
    if ($scope.currentLetter !== '') {
      if ($scope.currentLetter === $scope.word[$scope.index]) {
        $scope.index++;
        $scope.currentWord += $scope.currentLetter;
        $scope.log.push($scope.currentLetter+': Success!')
      }
      else {
        $scope.log.push($scope.currentLetter+': Try again!');
      }
    }
  };
}]);
