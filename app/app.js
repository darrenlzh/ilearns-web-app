var app = angular.module('iLearnsApp', ['ui.router', 'ngStorage', 'angularAudioRecorder']);

const SESSION_EXPIRE = {
  user: '',
  authenticated: false
};

app.controller('MainController', function($scope, $state, $sessionStorage) {
  $scope.storage = $sessionStorage;
  $scope.logout = function() {
    $sessionStorage.current = SESSION_EXPIRE;
    $state.transitionTo('login');
  }
});

app.run(function($rootScope, $location, $state, $sessionStorage, LoginService) {

  // Authentication check on page navigation
  $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams){

      // Check if Session is expired
      if (!$sessionStorage.current) {
        $sessionStorage.current = SESSION_EXPIRE;
      }

      LoginService.setCurrentUser($sessionStorage.current.user);
      LoginService.setAuth($sessionStorage.current.authenticated);

      // Check for authentication
      if(toState.authentication && !LoginService.isAuthenticated()) {

        // Page requires authn AND user is not authed
        $state.transitionTo('login');
        event.preventDefault();
      }
    });
  });

/*
**  UI Router and States:
**  - Route templates to ui-view based on current URL
**  - Can set authentication whether or not page is restricted
*/
app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/login');

  $stateProvider
    .state('login', {
      url: '/login',
      controller: 'LoginController',
      templateUrl: 'app/login/login.html'
    })
    .state('logout', {
      url: '/logout',
      templateUrl: '',
      redirectTo: 'login'
    })
    .state('app', {
      abstract: true,
      url: '',
      templateUrl: 'app/app.html'
    })
    .state('app.classroom', {
      url: '/classroom',
      controller: 'ClassroomController',
      templateUrl: 'app/classroom/classroom.html',
      authentication: true
    })
    .state('app.dashboard', {
      url: '/dashboard',
      controller: 'DashboardController',
      templateUrl: 'app/dashboard/dashboard.html',
      authentication: true
    })
    .state('app.words', {
      url: '/words',
      controller: 'WordsController',
      templateUrl: 'app/words/words.html',
      authentication: true
    })
    .state('game', {
      abstract: true,
      url: '',
      templateUrl: 'app/game/game.html'
    })
    .state('spelling', {
      url: '/game/spelling',
      controller: 'SpellingGameController',
      templateUrl: 'app/game/spelling/spelling.html',
      authentication: true
    });
});
