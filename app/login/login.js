app.controller('LoginController', function($scope, $rootScope, $stateParams, $state, $sessionStorage, LoginService) {
    $rootScope.title = "iLearns Login";

    $scope.formSubmit = function() {
      if(LoginService.login($scope.username, $scope.password)) {

        $scope.error = '';
        $scope.username = '';
        $scope.password = '';

        // Store user session when user is authenticated
        $sessionStorage.current = {
          user: LoginService.getCurrentUser(),
          authenticated: LoginService.isAuthenticated()
        };

        $state.transitionTo('app.dashboard');

      } else {
        $scope.error = "Incorrect username or password !";
        setTimeout(function() {
          $scope.error = '';
        }, 1000);
      }
    };
  });

  app.factory('LoginService', function($http, $sessionStorage) {
   var users = [
     { usr: 'admin', pwd: 'pass' },
     { usr: 'teacher', pwd: '1234' },
     { usr: 'darren', pwd: '1234' }
  ];
   var isAuthenticated = false;
   var currentUser = 'ADMIN';

   return {
     login : function(username, password) {

       for (i=0, len=users.length; i<len; i++) {
         if (username===users[i].usr && password===users[i].pwd) {
           isAuthenticated = true;
         }
       }
      //  isAuthenticated = username === admin && password === pass;
       if (isAuthenticated) currentUser = username;
       return isAuthenticated;
     },
     isAuthenticated : function() {
       return isAuthenticated;
     },
     getCurrentUser: function() {
       return currentUser;
     },
     setCurrentUser: function(user) {
       currentUser = user;
     },
     setAuth: function(authed) {
       isAuthenticated = authed;
     }
   };

 });
