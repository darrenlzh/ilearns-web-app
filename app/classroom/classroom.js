app.controller('ClassroomController', ['$scope', 'ClassroomService', function($scope, ClassroomService) {
  ClassroomService.then(function(data) {
    var result = data.data,
        studentList = result.students;
        class25 = [],
        class38 = [];
    for (let i=0; i<studentList.length; i++) {
      if (studentList[i].CLASS_ID === 25) {
        class25.push(studentList[i]);
      } else if (studentList[i].CLASS_ID === 38) {
        class38.push(studentList[i]);
      }
    }
    $scope.students = studentList;
    $scope.students25 = class25;
    $scope.students38 = class38;

    $scope.display = function(student, index) {
      $scope.item = index;
      $scope.showContent = true;
      $scope.toDisplay = student;
    }
  });
}]);

// var API = 'https://ilearns.nolanfoster.me/api/students/';
var API = 'app/test.json';

app.factory('ClassroomService', ['$http', function($http) {

  return $http.get(API).then(function(data) {
    return data;
  });
}]);
