app.controller('WordsController', ['$scope', 'WordService', 'orderByFilter',
  function($scope, WordService, orderBy) {

    $scope.recordLabel = 'Record';
    $scope.disablePlayBack = true;
    $scope.recordToggle = false;
    $scope.hasAudio = false;
    $scope.addWord = function(){
      
      //place word into with recording into database list

    }
    $scope.record = function(){


      $scope.recordToggle = !$scope.recordToggle;
      if ($scope.recordToggle) {
        $scope.recordLabel = 'Stop';
        $scope.disablePlayBack = true;
      } else {
        $scope.recordLabel = 'Record';
        if (!$scope.hasAudio) {
          $scope.disablePlayBack = false;
        }
      }
    }
    $scope.playBack = function(){
      //play audio file now
    }

    WordService.then(function(data) {
      var result = data.data;
      $scope.wordGroups = result.wordGroups;
      $scope.current = -1;
      $scope.currentWordGroup = {};
      $scope.currentWordist = [];
      $scope.show = false;

      $scope.propertyName = 'name';
      $scope.reverse = false;

      $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName===propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
        $scope.wordGroups = orderBy($scope.wordGroups, $scope.propertyName, $scope.reverse);
      };

      $scope.click = function(index) {
        $scope.current = index;
        $scope.showContent = true;
        $scope.currentWordGroup = $scope.wordGroups[index];
        $scope.currentWordList = $scope.wordGroups[index].wordList;
      };

      $scope.checkAll = function() {
        if ($scope.selectAll) {
          $scope.selectAll = true;
        }
        else {
          $scope.selectAll = false;
        }
        angular.forEach($scope.currentWordList, function (word) {
            word.selected = $scope.selectAll;
        });
      };
  });
}]);

var API2 = 'app/word_test.json';
app.factory('WordService', ['$http', function($http) {
  return $http.get(API2).then(function(data) {
    return data;
  });
}]);
