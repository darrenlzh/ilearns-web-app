app.controller('SpellingGameController', ['$scope', '$log',  'SpellingGameService', function($scope, $log, SpellingGameService) {
  SpellingGameService.then(
    function(data) {
      $scope.result = data.data;
      $scope.message = 'Successfully retrieved data!';
    },
    function(error) {
      $log.error('Unable to retrieve data...', error);
      $scope.message = 'Unable to retrieve data...';
    }
  );
  // $scope.currentLetter = '';
  // $scope.currentWord = '';
  // $scope.word = ['m', 'o', 'o', 'n'];
  // $scope.count = $scope.word.length;
  // $scope.index = 0;
  // $scope.log = [];
  $scope.wordList = ["DOG", "CAT", "MOON"];
  $scope.render = function() {
      // create instance of Phaser.Game.  Game has screen size: 852 x 480 (same size as background image)
      var game = new Phaser.Game(852, 480, Phaser.CANVAS, 'game', { preload: preload, create: create });

      // user input stored in this string
      var userInput = "";

      // We compare user input letter-by-letter to the current word from wordList.  If the currentWord is
      // "DOG" and this value is 1, we are comparing user input with the letter 'O'.
      var currentLetterIndex = 0;

      // Words that users will attempt to spell:
      var wordList = $scope.wordList;

      // Index of current word in wordList that users are currently attempting to spell.  Initially 0.
      var currentWordIndex = 0;

      // create variable currentWord and initialize to 0th element of wordList array.
      var currentWord = wordList[currentWordIndex];

      // Players array with player-numbers specified by strings.  The order of players here determines
      // the order in which players take turns.  Any player-number string added to this array must
      // have two corresponding images in the assets folder: <number>_red.png & <number>_green.png,
      var players = ["4", "2", "3", "1"];

      // Index of current player.  Corresponds to position in array players, NOT to the player-numbers.
      // Initialized to random integer between 0 and the (size of players array - 1), inclusive.
      // For example, if players array contains ["4", "2", "3", "1"] and currentPlayerIndex is initially
      // 2, it will be player "3"'s turn at the start of the game.
      var currentPlayerIndex = randomInteger(0, players.length - 1);

      // Player whose turn it is currently.  Type is string.
      var currentPlayer = players[currentPlayerIndex];

      // User images are stored in this array as objects instead of directly adding images to the scene
      // to allow their attributes to more easily be edited, such as visibility.  If it is a player's
      // turn, the green image corresponding to his number will be set visible=true, while the red
      // image of his number will be set visible=false.
      var userImageObjects = [];

      var try_again_currently_playing = 0;
      //var spell_current_word_currently_playing = 0;

      // This block checks for a key being pressed down by the user on the keyboard
      // and checks if this input is equal to the current letter of the current word
      // the player is trying to spell. For example, if the current word is "CAT" and
      // the current letter is 'A', we check the user's input against the letter 'A'.
      //
      // If the player has successfully entered the entire word in order correctly,
      // the program displays and plays audio "Great Job!" and
      // it becomes the next player's turn.  The new current word is the next word
      // in array wordList.
      //
      // If the player enters a wrong letter, a red X is displayed and the player is
      // told to "try again" and to "spell <currentWord>".
      document.onkeydown = function(event) {
          //incorrect.visible = false;
          //great_job.visible = false;
          var x = event.keyCode;
          var y = String.fromCharCode(x);
          if(y != currentWord.substring(currentLetterIndex, currentLetterIndex + 1)){
              if(try_again_currently_playing == 0 /* && spell_current_word_currently_playing == 0 */){
                  try_again_currently_playing = 1;
                  incorrect.visible = true;
                  currentLetterIndex = 0;
                  userInput = "";
                  userInputObject.text = "";
                  try_again();
                  window.setTimeout(function () {try_again_currently_playing = 0;}, 1300);
                  window.setTimeout(function () {incorrect.visible = false;}, 1000);
                  window.setTimeout(function () {spellCurrentWord();}, 1300);
                  //window.setTimeout(function () {spell_current_word_currently_playing = 0;}, 1300);
                  //spell_current_word_currently_playing = 1;
              }
              return;
          }
          currentLetterIndex++;
          userInput += y;
          userInputObject.text = userInput;
          if(userInput == currentWord){
              great_job_sound();
              great_job.visible = true;
              userInput = "";
              currentWordIndex++;
              currentLetterIndex = 0;
              if(currentWordIndex == wordList.length){
                  gameDone();
                  return;
              }
              currentWord = wordList[currentWordIndex];
              currentPlayerIndex = (currentPlayerIndex + 1) % 4;
              currentPlayer = players[currentPlayerIndex];
              window.setTimeout(function () { currentWordImage.kill(); updateCurrentWordImage(); updateUserImages()}, 2000);
              window.setTimeout(function () {great_job.visible = false;}, 2000);
              window.setTimeout(function () {userInputObject.text = "";}, 2000);
              window.setTimeout(function () {spellCurrentWord();}, 1300);
              return;
          }
      }

      function preload() {

          load_fixed_assets();
          load_dynamic_assets();
      }

      function load_fixed_assets(){

          game.load.image('background', 'assets/images/background.jpg');
          game.load.image('great_job', 'assets/images/great_job.jpg');
          game.load.image('incorrect', 'assets/images/incorrect.jpg');

          game.load.audio('spell', ['assets/sounds/spell.mp3']);
          game.load.audio('try', ['assets/sounds/try.mp3'])
          game.load.audio('again', ['assets/sounds/again.mp3'])
          game.load.audio('great', ['assets/sounds/great.mp3'])
          game.load.audio('job', ['assets/sounds/job.mp3'])

      }

      function load_dynamic_assets(){

          for(i = 0; i < wordList.length; i++){
              var next_word = wordList[i];
              var file_path_image = "assets/images/" + next_word + ".jpg";
              var file_path_sound = "assets/sounds/" + next_word + ".mp3";
              game.load.image(next_word, file_path_image);
              game.load.audio(next_word, [file_path_sound]);
          }

          for(i = 0; i < players.length; i++){
              var next_player = players[i];
              var file_path_player_red = "assets/images/" + next_player + "_red.png";
              var file_path_player_green = "assets/images/" + next_player + "_green.png";
              game.load.image(next_player + "_red", file_path_player_red);
              game.load.image(next_player + "_green", file_path_player_green);
          }
      }

      function create() {

      	game.add.sprite(0, 0, 'background');
          spelling_objects = game.add.group()
          updateCurrentWordImage();
          userInputObject = game.add.text(395, 125, userInput, { fontSize: '32px', fill: '#000'});
          great_job = spelling_objects.create(0, 0, 'great_job');
          great_job.scale.setTo(0.15, 0.15);
          great_job.visible = false;
          incorrect = spelling_objects.create(0, 0, 'incorrect');
          incorrect.scale.setTo(0.5, 0.5);
          incorrect.visible = false;
          spellCurrentWord();
          setupUserImages();
      }

      function spellCurrentWord() {
          game.sound.play('spell');
          window.setTimeout(function () {game.sound.play(currentWord);}, 1000);
      }

      function try_again() {
          game.sound.play('try');
          window.setTimeout(function () {game.sound.play('again');}, 300);
      }

      function great_job_sound() {
          game.sound.play('great');
          window.setTimeout(function () {game.sound.play('job');}, 500);
      }

      function gameDone(){

           window.setTimeout(function () {
              incorrect.visible = false;
              great_job.visible = false;
              userInputObject.visible = false;
              currentWordImage.visible = false;
              }, 2000);
      }

      function updateCurrentWordImage(){
          currentWordImage = spelling_objects.create(350, 240, currentWord);
          currentWordImage.scale.setTo(0.5, 0.5);
      }

      function setupUserImages(){
          for(i = 0; i < players.length; i++){
              userImageObjects[i] = spelling_objects.create(750, (i * 100) + 10, players[i] + "_red");
              userImageObjects[i].scale.setTo(0.25, 0.25);
              if(currentPlayerIndex == i){
                  userImageObjects[i].visible = false;
              }
          }

          for(i = 0; i < players.length; i++){
              userImageObjects[4 + i] = spelling_objects.create(750, (i * 100) + 10, players[i] + "_green");
              userImageObjects[4 + i].scale.setTo(0.25, 0.25);
              if(currentPlayerIndex != i){
                  userImageObjects[4 + i].visible = false;
              }
          }

      }

      function updateUserImages(){
          for(i = 0; i < players.length; i++){
              userImageObjects[i].visible = true;
              if(currentPlayerIndex == i){
                  userImageObjects[i].visible = false;
              }
          }

          for(i = 0; i < players.length; i++){
              userImageObjects[4 + i].visible = true;
              if(currentPlayerIndex != i){
                  userImageObjects[4 + i].visible = false;
              }
          }
      }

      // returns random integer in range [min, max].
      // note: Math.random() returns number in the range [0, 1)
      function randomInteger(min, max){
          return Math.floor(Math.random() * (max - min + 1)) + min;
      }
  } // END scope render


  // $scope.check = function(letter, e) {
  //   if ($scope.index === $scope.count) {
  //     alert('YAY!');
  //     $scope.index = 0;
  //     $scope.currentWord = '';
  //     $scope.log = [];
  //   }
  //   if ($scope.currentLetter !== '') {
  //     if ($scope.currentLetter === $scope.word[$scope.index]) {
  //       $scope.index++;
  //       $scope.currentWord += $scope.currentLetter;
  //       $scope.log.push($scope.currentLetter+': Success!')
  //     }
  //     else {
  //       $scope.log.push($scope.currentLetter+': Try again!');
  //     }
  //   }
  // };
}]);
